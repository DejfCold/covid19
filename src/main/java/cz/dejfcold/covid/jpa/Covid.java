package cz.dejfcold.covid.jpa;

import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "dataset")
public class Covid {
	@Id
	@GeneratedValue
	private UUID id;
	private Integer dead;
	private Integer infected;
	private Integer recovered;
	
	@Column(unique = true)
	private LocalDate date;
}
