package cz.dejfcold.covid.repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.dejfcold.covid.dto.CovidJpaDto;
import cz.dejfcold.covid.jpa.Covid;

public interface CovidRepository extends JpaRepository<Covid, UUID>{
	Collection<CovidJpaDto> findAllProjectedByOrderByDateAsc();
	Optional<Covid> findOneByDate(LocalDate date);
}
