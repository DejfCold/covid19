package cz.dejfcold.covid.csse;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Setter @Getter
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "dejfcold.scraper")
public class CsseScraperConfig {
	private String path;
	private String[] countries;
}
