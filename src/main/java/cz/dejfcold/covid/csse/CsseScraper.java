package cz.dejfcold.covid.csse;

import java.io.File;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import cz.dejfcold.covid.csv.CovidCsvDto;
import cz.dejfcold.covid.csv.CsvService;
import cz.dejfcold.covid.git.GitService;
import cz.dejfcold.covid.jpa.Covid;
import cz.dejfcold.covid.repository.CovidRepository;
import cz.dejfcold.covid.util.FileFilters;

@Service
public class CsseScraper implements Runnable {
	private static final DateTimeFormatter FILE_DATE_FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy'.csv'");
	private final GitService gitService;
	private final CsvService<CovidCsvDto> csvService;
	private final CovidRepository repository;
	private final CsseScraperConfig config;
	
	@Autowired
	public CsseScraper(
			GitService gitService, 
			CsvService<CovidCsvDto> csvService,
			CovidRepository repository,
			CsseScraperConfig config) {
		this.gitService = gitService;
		this.csvService = csvService;
		this.repository = repository;
		this.config = config;
	}
	
	@Override
	@Scheduled(cron = "0 0 * ? * *")
	public void run() {
		gitService.pull();
		String workTreePath = gitService.getWorkingDirectory();
		
		getCsvFileStream(workTreePath)
			.map(this::provideCovidCases)
			.forEach(repository::saveAll);
	}

	private List<? extends Covid> provideCovidCases(File file) {
		return csvService.read(file).stream()
			.filter(this::countryFilter)
			.map(convertToJpa(file))
			.collect(Collectors.toList());
	}

	private Function<? super CovidCsvDto, ? extends Covid> convertToJpa(File file) {
		var date = LocalDate.parse(file.getName(), FILE_DATE_FORMATTER);
		var covid = repository.findOneByDate(date).orElse(new Covid());
		return x -> {
			covid.setDate(date);
			covid.setDead(x.getDeaths());
			covid.setInfected(x.getConfirmed());
			covid.setRecovered(x.getRecovered());
			return covid;
		};
	}
	
	private boolean countryFilter(CovidCsvDto dto) {
		boolean passesFilter = false;
		if(dto.getCountryRegion() != null) {
			for(String country : config.getCountries()) {
				passesFilter |= dto.getCountryRegion().equalsIgnoreCase(country);
			}
		}
		return passesFilter;
	}
	
	private Stream<File> getCsvFileStream(String workTreePath) {
		File dir = new File(workTreePath + File.separator + config.getPath());
		File[] files = dir.listFiles(FileFilters.CSV_FILTER);
		return Arrays.stream(files);
	}
}
