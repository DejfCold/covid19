package cz.dejfcold.covid.csv;

import java.io.File;
import java.util.List;

public interface CsvService<T> {
	List<T> read(File file);
}
