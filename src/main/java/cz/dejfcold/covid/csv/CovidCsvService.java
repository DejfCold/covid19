package cz.dejfcold.covid.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Service
public class CovidCsvService implements CsvService<CovidCsvDto> {

	@Override
	public List<CovidCsvDto> read(File file) {
		try {
			return tryRead(file);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private List<CovidCsvDto> tryRead(File file) throws FileNotFoundException, IOException {
		try (Reader reader = new FileReader(file);
			CSVReader csvReader = new CSVReader(reader)) {

			CsvToBean<CovidCsvDto> csvToBean = new CsvToBeanBuilder<CovidCsvDto>(csvReader)
					.withType(CovidCsvDto.class)
					.build();
			List<CovidCsvDto> dtos = csvToBean.parse();
			return dtos;
		}
	}

}
