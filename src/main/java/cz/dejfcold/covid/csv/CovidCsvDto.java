package cz.dejfcold.covid.csv;

import com.opencsv.bean.CsvBindByName;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CovidCsvDto {
	@CsvBindByName(column = "Province/State")
	private String provinceStateSlash;
	@CsvBindByName(column = "Province_State")
	private String provinceStateUnderscore;
	
	@CsvBindByName(column = "Country/Region")
	private String countryRegionSlash;
	@CsvBindByName(column = "Country_Region")
	private String countryRegionUnderscore;
	
	@CsvBindByName(column = "Confirmed")
	private Integer confirmed;
	
	@CsvBindByName(column = "Deaths")
	private Integer deaths;
	
	@CsvBindByName(column = "Recovered")
	private Integer recovered;
	
	public String getProvinceState() {
		if(null != provinceStateSlash) {
			return provinceStateSlash;
		} else {
			return provinceStateUnderscore;
		}
	}
	
	public String getCountryRegion() {
		if(null != countryRegionSlash) {
			return countryRegionSlash;
		} else {
			return countryRegionUnderscore;
		}
	}
	
	public Integer getConfirmed() {
		return confirmed;
	}
	
	public Integer getDeaths() {
		return deaths;
	}
	
	public Integer getRecovered() {
		return recovered;
	}
	
}
