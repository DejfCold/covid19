package cz.dejfcold.covid.git;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "dejfcold.git")
public class CovidGitSupplierConfig {
	@NotNull private String repository;
}
