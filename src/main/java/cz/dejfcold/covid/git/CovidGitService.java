package cz.dejfcold.covid.git;

import org.eclipse.jgit.api.errors.CanceledException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidConfigurationException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.RefNotAdvertisedException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.api.errors.WrongRepositoryStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class CovidGitService implements GitService {
	private final GitSupplier gitSupplier;
	
	@Autowired
	public CovidGitService(GitSupplier gitSupplier) {
		this.gitSupplier = gitSupplier;
	}

	@Override
	public String getWorkingDirectory() {
		var path = gitSupplier.get().getRepository().getWorkTree().getAbsolutePath();
		log.info("Working directory path: {}", path);
		return path;
	}

	@Override
	public void pull() {
		try {
			tryPull();
		} catch (GitAPIException e) {
			throw new RuntimeException(e);
		}
	}

	private void tryPull() throws GitAPIException, WrongRepositoryStateException, InvalidConfigurationException,
			InvalidRemoteException, CanceledException, RefNotFoundException, RefNotAdvertisedException, NoHeadException,
			TransportException {
		log.info("Pulling ...");
		gitSupplier.get().pull().call();
	}

}
