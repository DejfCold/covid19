package cz.dejfcold.covid.git;

import java.util.function.Supplier;

import org.eclipse.jgit.api.Git;
import org.springframework.stereotype.Service;

@Service
public interface GitSupplier extends Supplier<Git>{
	@Override
	Git get();
}
