package cz.dejfcold.covid.git;

import java.io.File;
import java.io.IOException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CovidGitSupplier implements GitSupplier {
	private final CovidGitSupplierConfig config;
	
	@Autowired
	CovidGitSupplier(CovidGitSupplierConfig config) {
		this.config = config;
	}

	@Override
	public Git get() {
		try {
			return tryGet();
		} catch (GitAPIException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Git tryGet() throws InvalidRemoteException, TransportException, GitAPIException {
		File directory = new File("repository");
		try {
			return Git.open(directory);
		} catch (IOException e) {
			return Git
					.cloneRepository()
					.setURI(config.getRepository())
					.setDirectory(directory)
					.call();
		}
	}
	

}
