package cz.dejfcold.covid.git;

import org.springframework.stereotype.Service;

@Service
public interface GitService {
	String getWorkingDirectory();
	void pull();
}
