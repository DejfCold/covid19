package cz.dejfcold.covid.dto;

import java.time.LocalDate;
import lombok.Value;

@Value
public class CovidJpaDto {
	Integer dead;
	Integer infected;
	Integer recovered;
	LocalDate date;
}
