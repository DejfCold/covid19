package cz.dejfcold.covid.api;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.dejfcold.covid.csse.CsseScraper;
import cz.dejfcold.covid.dto.CovidJpaDto;
import cz.dejfcold.covid.repository.CovidRepository;

@RestController
@RequestMapping("/api/data")
public class Data {
	
	private final CovidRepository covidRepository;
	private final CsseScraper scraper;
	@Autowired
	public Data(CovidRepository covidRepository,
			CsseScraper scraper) {
		this.covidRepository = covidRepository;
		this.scraper = scraper;
	}

	@GetMapping
	public Collection<CovidJpaDto> getData() {
		return covidRepository.findAllProjectedByOrderByDateAsc();
	}
	
	@GetMapping("/scrape")
	public void scrape() {
		scraper.run();
	}
}
