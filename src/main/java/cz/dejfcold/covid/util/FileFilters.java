package cz.dejfcold.covid.util;

import java.io.FileFilter;

public final class FileFilters {
	public static final FileFilter CSV_FILTER = x -> x.getName().toLowerCase().endsWith(".csv");
}
