package cz.dejfcold.covid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class CovidGraphCzechApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidGraphCzechApplication.class, args);
	}

}
