-- Table: public.dataset

-- DROP TABLE public.dataset;

CREATE TABLE public.dataset
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    dead numeric,
    infected numeric,
    recovered numeric,
    date date NOT NULL,
    CONSTRAINT "covid-dataset_pkey" PRIMARY KEY (id),
    CONSTRAINT "unique-date" UNIQUE (date)

)

TABLESPACE pg_default;

ALTER TABLE public.dataset
    OWNER to postgres;