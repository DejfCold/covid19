-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Contact: mailto:dejfcold@dejfcold.cz
Contact: https://twitter.com/DejfCold
Encryption: https://covid.dejfcold.cz/.well-known/publickey.dejfcold@dejfcold.cz.asc
Acknowledgments: https://twitter.com/spazef0rze
Preferred-Languages: cz, en
Canonical: https://covid.dejfcold.cz/.well-known/security.txt
-----BEGIN PGP SIGNATURE-----

iQFJBAEBCAAzFiEEkFPSKma7KcY3zGPD7k9pSvDB74wFAl6BPAIVHGRlamZjb2xk
QGRlamZjb2xkLmN6AAoJEO5PaUrwwe+MTikH/ifPr77YnVaeAVjqxzcqUC0gjaYt
XZQvIuqdoI04eRz19lh+jZ95xQkjzh77p0kfy4PKzAKPZsOYF9OYByJ8Av7dWfM/
YkNtvqz1xaibg68gmJFfkmR+i+J2YqTV9C4g6bvi7GECYZE9XiNXUHATySWC1A3C
HfqE5kn+t/VLRCogVrNn0Xq41KfTbgqNBbNR1uGgw+YfZnKD6/arsQ0S/GWRsEiC
pQBkNKTmQzJycwMmrOdCbpdca3h4PPI+7YT6g5ynZ/ycCsRWWzf1XdcJRVQiw8CB
0Y1j+V9aYu7MAA9gAWsWtcOlZt6o1Hozea2gG0yJQnZM5WWYOIIx/49iz9k=
=FLis
-----END PGP SIGNATURE-----
